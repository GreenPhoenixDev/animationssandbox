﻿using UnityEngine;
using DG.Tweening;

public class MovingCrystal : MonoBehaviour
{
	[SerializeField] private Transform crystal;
	[SerializeField] private float duration = 1f;
	[SerializeField] private Vector3 offset;

	void OnMouseDown()
	{
		crystal.DOMove(transform.position + offset, duration);
	}
}
