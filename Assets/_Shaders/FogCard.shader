﻿Shader "Unlit/FogCard"
{
    Properties
    {
		[Header(Textures and Color)]
		[Space]
        _MainTex ("Fog texture", 2D) = "white" {}
		_Mask ("Mask", 2D) = "white" {}
		_Color ("Color", Color) = (1.0, 1.0, 1.0, 1.0)
		[Space(10)]

		[Header(Behaviour)]
		[Space]
		_ScrollDirX ("Scroll along X", Range(-100.0, 100.0)) = 1.0
		_ScrollDirY ("Scroll along Y", Range(-100.0, 100.0)) = 1.0
		_Speed ("Scroll speed", Float) = 1.0
		_Distance ("Fading distance ", Range(1.0, 100.0)) = 1.0
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha 
		ZWrite Off
		Cull Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct v2f
            {
                float4 pos: SV_POSITION;
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;

            v2f vert (appdata_full v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.uv2 = v.texcoord;
                return o;
            }

			float _Distance;
			sampler2D _Mask;
			float _Speed;
			fixed _ScrollDirX;
			fixed _ScrollDirY;
			fixed4 _Color;

			fixed4 frag(v2f i) : SV_Target
			{
				float2 uv = i.uv + fixed2(_ScrollDirX, _ScrollDirY) * _Speed * _Time.x;
                fixed4 col = tex2D(_MainTex, uv) * _Color;
				col.a *= tex2D(_Mask, i.uv2);
				col.a *= saturate(1 - ((i.pos.z / i.pos.w) * _Distance));

                return col;
            }
            ENDCG
        }
    }
}
