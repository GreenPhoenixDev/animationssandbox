﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinFloater : MonoBehaviour
{
	[SerializeField] [Range(0f, 1f)]private float coinFloatingStrength;
	private Animator anim;

    void Start()
    {
		anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
		anim.SetLayerWeight(1, coinFloatingStrength);
    }
}
